import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import { Button } from '@tarojs/components'
import { AtButton, AtAvatar } from 'taro-ui'
import './index.scss'

export default class Index extends Component {

  config = {
    navigationBarTitleText: '首页'
  }

  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  showClick = ()=>{
    alert(11)
  }

  render() {
    return (
      <View className='index'>
        <Text>Hello world!</Text>
        <Button><Text>242</Text></Button>
        <AtButton type='primary' className='btn' onClick={this.showClick}>按钮文案</AtButton>
        <AtAvatar image='https://jdc.jd.com/img/200' circle></AtAvatar>
        <View className='at-article'>
          <View className='at-article__h1'>
            这是一级标题这是一级标题
          </View>
          <View className='at-article__info'>
            2017-05-07&nbsp;&nbsp;&nbsp;这是作者
          </View>
          <View className='at-article__content'>
            <View className='at-article__section'>
              <View className='at-article__h2'>这是二级标题</View>
              <View className='at-article__h3'>这是三级标题</View>
              <View className='at-article__p'>
                这是文本段落。这是文本段落。这是文本段落。这是文本段落。这是文本段落。这是文本段落。这是文本段落。这是文本落。这是文本段落。1234567890123456789012345678901234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZ
              </View>
              <View className='at-article__p'>
                这是文本段落。这是文本段落。
              </View>
              <Image
                className='at-article__img'
                src='https://jdc.jd.com/img/400x400'
                mode='widthFix' />
            </View>
          </View>
        </View>
      </View>

    )
  }
}
